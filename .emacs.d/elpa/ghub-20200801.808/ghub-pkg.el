(define-package "ghub" "20200801.808" "Minuscule client libraries for Git forge APIs."
  '((emacs "25.1")
    (let-alist "1.0.5")
    (treepy "0.1.1"))
  :commit "ae5b70bbbe30e495517417dc0700316b2daf6e6a")
;; Local Variables:
;; no-byte-compile: t
;; End:
