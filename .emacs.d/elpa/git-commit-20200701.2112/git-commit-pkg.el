;;; -*- no-byte-compile: t -*-
(define-package "git-commit" "20200701.2112" "Edit Git commit messages" '((emacs "25.1") (dash "20200524") (transient "20200601") (with-editor "20200522")) :commit "798aff56457aef4d1896db8129ab56d08ae12066" :keywords '("git" "tools" "vc") :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/magit/magit")
