;;; org-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads "actual autoloads are elsewhere" "ob-C" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-C.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-C.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-C" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-J" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-J.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-J.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-J" '("obj-" "org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-R" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-R.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-R.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-R" '("ob-R-" "org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-abc" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-abc.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-abc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-abc" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-asymptote"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-asymptote.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-asymptote.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-asymptote" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-awk" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-awk.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-awk.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-awk" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-calc" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-calc.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-calc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-calc" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-clojure" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-clojure.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-clojure.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-clojure" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-comint" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-comint.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-comint.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-comint" '("org-babel-comint-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-coq" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-coq.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-coq.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-coq" '("org-babel-" "coq-program-name")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-core" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-core.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-core.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-core" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-css" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-css.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-css.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-css" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-ditaa" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ditaa.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-ditaa.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ditaa" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-dot" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-dot.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-dot.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-dot" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-ebnf" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ebnf.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-ebnf.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ebnf" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-emacs-lisp"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-emacs-lisp.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-emacs-lisp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-emacs-lisp" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-eshell" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-eshell.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-eshell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-eshell" '("ob-eshell-session-live-p" "org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-eval" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-eval.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-eval.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-eval" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-exp" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-exp.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-exp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-exp" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-forth" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-forth.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-forth.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-forth" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-fortran" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-fortran.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-fortran.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-fortran" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-gnuplot" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-gnuplot.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-gnuplot.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-gnuplot" '("org-babel-" "*org-babel-gnuplot-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-groovy" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-groovy.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-groovy.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-groovy" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-haskell" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-haskell.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-haskell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-haskell" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-hledger" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-hledger.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-hledger.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-hledger" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-io" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-io.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-io.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-io" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-java" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-java.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-java.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-java" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-js" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-js.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-js.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-js" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-latex" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-latex.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-latex.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-latex" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-ledger" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ledger.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-ledger.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ledger" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-lilypond"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-lilypond.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-lilypond.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-lilypond" '("org-babel-" "lilypond-mode")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-lisp" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-lisp.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-lisp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-lisp" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-lob" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-lob.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-lob.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-lob" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-lua" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-lua.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-lua.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-lua" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-makefile"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-makefile.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-makefile.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-makefile" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-maxima" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-maxima.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-maxima.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-maxima" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-mscgen" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-mscgen.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-mscgen.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-mscgen" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-ocaml" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ocaml.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-ocaml.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ocaml" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-octave" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-octave.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-octave.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-octave" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-org" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-org.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-org.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-org" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-perl" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-perl.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-perl.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-perl" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-picolisp"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-picolisp.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-picolisp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-picolisp" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-plantuml"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-plantuml.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-plantuml.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-plantuml" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-processing"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-processing.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-processing.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-processing" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-python" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-python.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-python.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-python" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-ref" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ref.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-ref.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ref" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-ruby" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ruby.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-ruby.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ruby" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-sass" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-sass.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-sass.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-sass" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-scheme" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-scheme.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-scheme.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-scheme" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-screen" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-screen.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-screen.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-screen" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-sed" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-sed.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-sed.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-sed" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-shell" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-shell.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-shell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-shell" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-shen" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-shen.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-shen.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-shen" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-sql" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-sql.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-sql.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-sql" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-sqlite" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-sqlite.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-sqlite.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-sqlite" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-stan" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-stan.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-stan.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-stan" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-table" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-table.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-table.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-table" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-tangle" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-tangle.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-tangle.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-tangle" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-vala" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-vala.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ob-vala.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-vala" '("org-babel-")))

;;;***

;;;### (autoloads nil "ol" "../../../../../../.emacs.d/elpa/org-9.3.6/ol.el"
;;;;;;  "5abc2a674bb1e2a52817563ce0a929d2")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol.el

(autoload 'org-next-link "ol" "\
Move forward to the next link.
If the link is in hidden text, expose it.  When SEARCH-BACKWARD
is non-nil, move backward.

\(fn &optional SEARCH-BACKWARD)" t nil)

(autoload 'org-previous-link "ol" "\
Move backward to the previous link.
If the link is in hidden text, expose it.

\(fn)" t nil)

(autoload 'org-toggle-link-display "ol" "\
Toggle the literal or descriptive display of links.

\(fn)" t nil)

(autoload 'org-store-link "ol" "\
Store a link to the current location.
\\<org-mode-map>
This link is added to `org-stored-links' and can later be inserted
into an Org buffer with `org-insert-link' (`\\[org-insert-link]').

For some link types, a `\\[universal-argument]' prefix ARG is interpreted.  A single
`\\[universal-argument]' negates `org-context-in-file-links' for file links or
`org-gnus-prefer-web-links' for links to Usenet articles.

A `\\[universal-argument] \\[universal-argument]' prefix ARG forces skipping storing functions that are not
part of Org core.

A `\\[universal-argument] \\[universal-argument] \\[universal-argument]' prefix ARG forces storing a link for each line in the
active region.

Assume the function is called interactively if INTERACTIVE? is
non-nil.

\(fn ARG &optional INTERACTIVE\\=\\?)" t nil)

(autoload 'org-insert-link "ol" "\
Insert a link.  At the prompt, enter the link.

Completion can be used to insert any of the link protocol prefixes in use.

The history can be used to select a link previously stored with
`org-store-link'.  When the empty string is entered (i.e. if you just
press `RET' at the prompt), the link defaults to the most recently
stored link.  As `SPC' triggers completion in the minibuffer, you need to
use `M-SPC' or `C-q SPC' to force the insertion of a space character.

You will also be prompted for a description, and if one is given, it will
be displayed in the buffer instead of the link.

If there is already a link at point, this command will allow you to edit
link and description parts.

With a `\\[universal-argument]' prefix, prompts for a file to link to.  The file name can be
selected using completion.  The path to the file will be relative to the
current directory if the file is in the current directory or a subdirectory.
Otherwise, the link will be the absolute path as completed in the minibuffer
\(i.e. normally ~/path/to/file).  You can configure this behavior using the
option `org-link-file-path-type'.

With a `\\[universal-argument] \\[universal-argument]' prefix, enforce an absolute path even if the file is in
the current directory or below.

A `\\[universal-argument] \\[universal-argument] \\[universal-argument]' prefix negates `org-link-keep-stored-after-insertion'.

If the LINK-LOCATION parameter is non-nil, this value will be used as
the link location instead of reading one interactively.

If the DESCRIPTION parameter is non-nil, this value will be used as the
default description.  Otherwise, if `org-link-make-description-function'
is non-nil, this function will be called with the link target, and the
result will be the default link description.  When called non-interactively,
don't allow to edit the default description.

\(fn &optional COMPLETE-FILE LINK-LOCATION DESCRIPTION)" t nil)

(autoload 'org-insert-all-links "ol" "\
Insert all links in `org-stored-links'.
When a universal prefix, do not delete the links from `org-stored-links'.
When `ARG' is a number, insert the last N link(s).
`PRE' and `POST' are optional arguments to define a string to
prepend or to append.

\(fn ARG &optional PRE POST)" t nil)

(autoload 'org-insert-last-stored-link "ol" "\
Insert the last link stored in `org-stored-links'.

\(fn ARG)" t nil)

(autoload 'org-insert-link-global "ol" "\
Insert a link like Org mode does.
This command can be called in any mode to insert a link in Org syntax.

\(fn)" t nil)

(autoload 'org-update-radio-target-regexp "ol" "\
Find all radio targets in this file and update the regular expression.
Also refresh fontification if needed.

\(fn)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "ol" "../../../../../../.emacs.d/elpa/org-9.3.6/ol.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol" '("org-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-bbdb" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-bbdb.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-bbdb.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-bbdb" '("org-bbdb-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-bibtex" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-bibtex.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-bibtex.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-bibtex" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-docview" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-docview.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-docview.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-docview" '("org-docview-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-eshell" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-eshell.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-eshell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-eshell" '("org-eshell-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-eww" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-eww.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-eww.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-eww" '("org-eww-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-gnus" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-gnus.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-gnus.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-gnus" '("org-gnus-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-info" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-info.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-info.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-info" '("org-info-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-irc" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-irc.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-irc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-irc" '("org-irc-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-mhe" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-mhe.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-mhe.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-mhe" '("org-mhe-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-rmail" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-rmail.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-rmail.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-rmail" '("org-rmail-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ol-w3m" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-w3m.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ol-w3m.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ol-w3m" '("org-w3m-")))

;;;***

;;;### (autoloads nil "org" "../../../../../../.emacs.d/elpa/org-9.3.6/org.el"
;;;;;;  "dee248a27c37868733cd1f62bf163653")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org.el

(autoload 'org-babel-do-load-languages "org" "\
Load the languages defined in `org-babel-load-languages'.

\(fn SYM VALUE)" nil nil)

(autoload 'org-babel-load-file "org" "\
Load Emacs Lisp source code blocks in the Org FILE.
This function exports the source code using `org-babel-tangle'
and then loads the resulting file using `load-file'.  With
optional prefix argument COMPILE, the tangled Emacs Lisp file is
byte-compiled before it is loaded.

\(fn FILE &optional COMPILE)" t nil)

(autoload 'org-version "org" "\
Show the Org version.
Interactively, or when MESSAGE is non-nil, show it in echo area.
With prefix argument, or when HERE is non-nil, insert it at point.
In non-interactive uses, a reduced version string is output unless
FULL is given.

\(fn &optional HERE FULL MESSAGE)" t nil)

(autoload 'org-clock-persistence-insinuate "org" "\
Set up hooks for clock persistence.

\(fn)" nil nil)

(autoload 'org-mode "org" "\
Outline-based notes management and organizer, alias
\"Carsten's outline-mode for keeping track of everything.\"

Org mode develops organizational tasks around a NOTES file which
contains information about projects as plain text.  Org mode is
implemented on top of Outline mode, which is ideal to keep the content
of large files well structured.  It supports ToDo items, deadlines and
time stamps, which magically appear in the diary listing of the Emacs
calendar.  Tables are easily created with a built-in table editor.
Plain text URL-like links connect to websites, emails (VM), Usenet
messages (Gnus), BBDB entries, and any files related to the project.
For printing and sharing of notes, an Org file (or a part of it)
can be exported as a structured ASCII or HTML file.

The following commands are available:

\\{org-mode-map}

\(fn)" t nil)

(autoload 'org-cycle "org" "\
TAB-action and visibility cycling for Org mode.

This is the command invoked in Org mode by the `TAB' key.  Its main
purpose is outline visibility cycling, but it also invokes other actions
in special contexts.

When this function is called with a `\\[universal-argument]' prefix, rotate the entire
buffer through 3 states (global cycling)
  1. OVERVIEW: Show only top-level headlines.
  2. CONTENTS: Show all headlines of all levels, but no body text.
  3. SHOW ALL: Show everything.

With a `\\[universal-argument] \\[universal-argument]' prefix argument, switch to the startup visibility,
determined by the variable `org-startup-folded', and by any VISIBILITY
properties in the buffer.

With a `\\[universal-argument] \\[universal-argument] \\[universal-argument]' prefix argument, show the entire buffer, including
any drawers.

When inside a table, re-align the table and move to the next field.

When point is at the beginning of a headline, rotate the subtree started
by this line through 3 different states (local cycling)
  1. FOLDED:   Only the main headline is shown.
  2. CHILDREN: The main headline and the direct children are shown.
               From this state, you can move to one of the children
               and zoom in further.
  3. SUBTREE:  Show the entire subtree, including body text.
If there is no subtree, switch directly from CHILDREN to FOLDED.

When point is at the beginning of an empty headline and the variable
`org-cycle-level-after-item/entry-creation' is set, cycle the level
of the headline by demoting and promoting it to likely levels.  This
speeds up creation document structure by pressing `TAB' once or several
times right after creating a new headline.

When there is a numeric prefix, go up to a heading with level ARG, do
a `show-subtree' and return to the previous cursor position.  If ARG
is negative, go up that many levels.

When point is not at the beginning of a headline, execute the global
binding for `TAB', which is re-indenting the line.  See the option
`org-cycle-emulate-tab' for details.

As a special case, if point is at the beginning of the buffer and there is
no headline in line 1, this function will act as if called with prefix arg
\(`\\[universal-argument] TAB', same as `S-TAB') also when called without prefix arg, but only
if the variable `org-cycle-global-at-bob' is t.

\(fn &optional ARG)" t nil)

(autoload 'org-global-cycle "org" "\
Cycle the global visibility.  For details see `org-cycle'.
With `\\[universal-argument]' prefix ARG, switch to startup visibility.
With a numeric prefix, show all headlines up to that level.

\(fn &optional ARG)" t nil)

(autoload 'org-run-like-in-org-mode "org" "\
Run a command, pretending that the current buffer is in Org mode.
This will temporarily bind local variables that are typically bound in
Org mode to the values they have in Org mode, and then interactively
call CMD.

\(fn CMD)" nil nil)

(autoload 'org-open-file "org" "\
Open the file at PATH.
First, this expands any special file name abbreviations.  Then the
configuration variable `org-file-apps' is checked if it contains an
entry for this file type, and if yes, the corresponding command is launched.

If no application is found, Emacs simply visits the file.

With optional prefix argument IN-EMACS, Emacs will visit the file.
With a double \\[universal-argument] \\[universal-argument] prefix arg, Org tries to avoid opening in Emacs
and to use an external application to visit the file.

Optional LINE specifies a line to go to, optional SEARCH a string
to search for.  If LINE or SEARCH is given, the file will be
opened in Emacs, unless an entry from `org-file-apps' that makes
use of groups in a regexp matches.

If you want to change the way frames are used when following a
link, please customize `org-link-frame-setup'.

If the file does not exist, throw an error.

\(fn PATH &optional IN-EMACS LINE SEARCH)" nil nil)

(autoload 'org-open-at-point-global "org" "\
Follow a link or a time-stamp like Org mode does.
Also follow links and emails as seen by `thing-at-point'.
This command can be called in any mode to follow an external
link or a time-stamp that has Org mode syntax.  Its behavior
is undefined when called on internal links like fuzzy links.
Raise a user error when there is nothing to follow.

\(fn)" t nil)

(autoload 'org-offer-links-in-entry "org" "\
Offer links in the current entry and return the selected link.
If there is only one link, return it.
If NTH is an integer, return the NTH link found.
If ZERO is a string, check also this string for a link, and if
there is one, return it.

\(fn BUFFER MARKER &optional NTH ZERO)" nil nil)

(autoload 'org-switchb "org" "\
Switch between Org buffers.

With `\\[universal-argument]' prefix, restrict available buffers to files.

With `\\[universal-argument] \\[universal-argument]' prefix, restrict available buffers to agenda files.

\(fn &optional ARG)" t nil)

(autoload 'org-cycle-agenda-files "org" "\
Cycle through the files in `org-agenda-files'.
If the current buffer visits an agenda file, find the next one in the list.
If the current buffer does not, find the first agenda file.

\(fn)" t nil)

(autoload 'org-submit-bug-report "org" "\
Submit a bug report on Org via mail.

Don't hesitate to report any problems or inaccurate documentation.

If you don't have setup sending mail from (X)Emacs, please copy the
output buffer into your mail program, as it gives us important
information about your Org version and configuration.

\(fn)" t nil)

(autoload 'org-reload "org" "\
Reload all Org Lisp files.
With prefix arg UNCOMPILED, load the uncompiled versions.

\(fn &optional UNCOMPILED)" t nil)

(autoload 'org-customize "org" "\
Call the customize function with org as argument.

\(fn)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org" "../../../../../../.emacs.d/elpa/org-9.3.6/org.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org" '("org-" "turn-on-org-cdlatex")))

;;;***

;;;***

;;;### (autoloads nil "org-agenda" "../../../../../../.emacs.d/elpa/org-9.3.6/org-agenda.el"
;;;;;;  "35283ed9936e1b44bc9b29bb2e31538b")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-agenda.el

(autoload 'org-toggle-sticky-agenda "org-agenda" "\
Toggle `org-agenda-sticky'.

\(fn &optional ARG)" t nil)

(autoload 'org-agenda "org-agenda" "\
Dispatch agenda commands to collect entries to the agenda buffer.
Prompts for a command to execute.  Any prefix arg will be passed
on to the selected command.  The default selections are:

a     Call `org-agenda-list' to display the agenda for current day or week.
t     Call `org-todo-list' to display the global todo list.
T     Call `org-todo-list' to display the global todo list, select only
      entries with a specific TODO keyword (the user gets a prompt).
m     Call `org-tags-view' to display headlines with tags matching
      a condition  (the user is prompted for the condition).
M     Like `m', but select only TODO entries, no ordinary headlines.
e     Export views to associated files.
s     Search entries for keywords.
S     Search entries for keywords, only with TODO keywords.
/     Multi occur across all agenda files and also files listed
      in `org-agenda-text-search-extra-files'.
<     Restrict agenda commands to buffer, subtree, or region.
      Press several times to get the desired effect.
>     Remove a previous restriction.
#     List \"stuck\" projects.
!     Configure what \"stuck\" means.
C     Configure custom agenda commands.

More commands can be added by configuring the variable
`org-agenda-custom-commands'.  In particular, specific tags and TODO keyword
searches can be pre-defined in this way.

If the current buffer is in Org mode and visiting a file, you can also
first press `<' once to indicate that the agenda should be temporarily
\(until the next use of `\\[org-agenda]') restricted to the current file.
Pressing `<' twice means to restrict to the current subtree or region
\(if active).

\(fn &optional ARG ORG-KEYS RESTRICTION)" t nil)

(autoload 'org-batch-agenda "org-agenda" "\
Run an agenda command in batch mode and send the result to STDOUT.
If CMD-KEY is a string of length 1, it is used as a key in
`org-agenda-custom-commands' and triggers this command.  If it is a
longer string it is used as a tags/todo match string.
Parameters are alternating variable names and values that will be bound
before running the agenda command.

\(fn CMD-KEY &rest PARAMETERS)" nil t)

(autoload 'org-batch-agenda-csv "org-agenda" "\
Run an agenda command in batch mode and send the result to STDOUT.
If CMD-KEY is a string of length 1, it is used as a key in
`org-agenda-custom-commands' and triggers this command.  If it is a
longer string it is used as a tags/todo match string.
Parameters are alternating variable names and values that will be bound
before running the agenda command.

The output gives a line for each selected agenda item.  Each
item is a list of comma-separated values, like this:

category,head,type,todo,tags,date,time,extra,priority-l,priority-n

category     The category of the item
head         The headline, without TODO kwd, TAGS and PRIORITY
type         The type of the agenda entry, can be
                todo               selected in TODO match
                tagsmatch          selected in tags match
                diary              imported from diary
                deadline           a deadline on given date
                scheduled          scheduled on given date
                timestamp          entry has timestamp on given date
                closed             entry was closed on given date
                upcoming-deadline  warning about deadline
                past-scheduled     forwarded scheduled item
                block              entry has date block including g. date
todo         The todo keyword, if any
tags         All tags including inherited ones, separated by colons
date         The relevant date, like 2007-2-14
time         The time, like 15:00-16:50
extra        String with extra planning info
priority-l   The priority letter if any was given
priority-n   The computed numerical priority
agenda-day   The day in the agenda where this is listed

\(fn CMD-KEY &rest PARAMETERS)" nil t)

(autoload 'org-store-agenda-views "org-agenda" "\
Store agenda views.

\(fn &rest PARAMETERS)" t nil)

(autoload 'org-batch-store-agenda-views "org-agenda" "\
Run all custom agenda commands that have a file argument.

\(fn &rest PARAMETERS)" nil t)

(autoload 'org-agenda-list "org-agenda" "\
Produce a daily/weekly view from all files in variable `org-agenda-files'.
The view will be for the current day or week, but from the overview buffer
you will be able to go to other days/weeks.

With a numeric prefix argument in an interactive call, the agenda will
span ARG days.  Lisp programs should instead specify SPAN to change
the number of days.  SPAN defaults to `org-agenda-span'.

START-DAY defaults to TODAY, or to the most recent match for the weekday
given in `org-agenda-start-on-weekday'.

When WITH-HOUR is non-nil, only include scheduled and deadline
items if they have an hour specification like [h]h:mm.

\(fn &optional ARG START-DAY SPAN WITH-HOUR)" t nil)

(autoload 'org-search-view "org-agenda" "\
Show all entries that contain a phrase or words or regular expressions.

With optional prefix argument TODO-ONLY, only consider entries that are
TODO entries.  The argument STRING can be used to pass a default search
string into this function.  If EDIT-AT is non-nil, it means that the
user should get a chance to edit this string, with cursor at position
EDIT-AT.

The search string can be viewed either as a phrase that should be found as
is, or it can be broken into a number of snippets, each of which must match
in a Boolean way to select an entry.  The default depends on the variable
`org-agenda-search-view-always-boolean'.
Even if this is turned off (the default) you can always switch to
Boolean search dynamically by preceding the first word with  \"+\" or \"-\".

The default is a direct search of the whole phrase, where each space in
the search string can expand to an arbitrary amount of whitespace,
including newlines.

If using a Boolean search, the search string is split on whitespace and
each snippet is searched separately, with logical AND to select an entry.
Words prefixed with a minus must *not* occur in the entry.  Words without
a prefix or prefixed with a plus must occur in the entry.  Matching is
case-insensitive.  Words are enclosed by word delimiters (i.e. they must
match whole words, not parts of a word) if
`org-agenda-search-view-force-full-words' is set (default is nil).

Boolean search snippets enclosed by curly braces are interpreted as
regular expressions that must or (when preceded with \"-\") must not
match in the entry.  Snippets enclosed into double quotes will be taken
as a whole, to include whitespace.

- If the search string starts with an asterisk, search only in headlines.
- If (possibly after the leading star) the search string starts with an
  exclamation mark, this also means to look at TODO entries only, an effect
  that can also be achieved with a prefix argument.
- If (possibly after star and exclamation mark) the search string starts
  with a colon, this will mean that the (non-regexp) snippets of the
  Boolean search must match as full words.

This command searches the agenda files, and in addition the files
listed in `org-agenda-text-search-extra-files' unless a restriction lock
is active.

\(fn &optional TODO-ONLY STRING EDIT-AT)" t nil)

(autoload 'org-todo-list "org-agenda" "\
Show all (not done) TODO entries from all agenda file in a single list.
The prefix arg can be used to select a specific TODO keyword and limit
the list to these.  When using `\\[universal-argument]', you will be prompted
for a keyword.  A numeric prefix directly selects the Nth keyword in
`org-todo-keywords-1'.

\(fn &optional ARG)" t nil)

(autoload 'org-tags-view "org-agenda" "\
Show all headlines for all `org-agenda-files' matching a TAGS criterion.
The prefix arg TODO-ONLY limits the search to TODO entries.

\(fn &optional TODO-ONLY MATCH)" t nil)

(autoload 'org-agenda-list-stuck-projects "org-agenda" "\
Create agenda view for projects that are stuck.
Stuck projects are project that have no next actions.  For the definitions
of what a project is and how to check if it stuck, customize the variable
`org-stuck-projects'.

\(fn &rest IGNORE)" t nil)

(autoload 'org-diary "org-agenda" "\
Return diary information from org files.
This function can be used in a \"sexp\" diary entry in the Emacs calendar.
It accesses org files and extracts information from those files to be
listed in the diary.  The function accepts arguments specifying what
items should be listed.  For a list of arguments allowed here, see the
variable `org-agenda-entry-types'.

The call in the diary file should look like this:

   &%%(org-diary) ~/path/to/some/orgfile.org

Use a separate line for each org file to check.  Or, if you omit the file name,
all files listed in `org-agenda-files' will be checked automatically:

   &%%(org-diary)

If you don't give any arguments (as in the example above), the default value
of `org-agenda-entry-types' is used: (:deadline :scheduled :timestamp :sexp).
So the example above may also be written as

   &%%(org-diary :deadline :timestamp :sexp :scheduled)

The function expects the lisp variables `entry' and `date' to be provided
by the caller, because this is how the calendar works.  Don't use this
function from a program - use `org-agenda-get-day-entries' instead.

\(fn &rest ARGS)" nil nil)

(autoload 'org-agenda-check-for-timestamp-as-reason-to-ignore-todo-item "org-agenda" "\
Do we have a reason to ignore this TODO entry because it has a time stamp?

\(fn &optional END)" nil nil)

(autoload 'org-agenda-set-restriction-lock "org-agenda" "\
Set restriction lock for agenda to current subtree or file.
When in a restricted subtree, remove it.

The restriction will span over the entire file if TYPE is `file',
or if type is '(4), or if the cursor is before the first headline
in the file. Otherwise, only apply the restriction to the current
subtree.

\(fn &optional TYPE)" t nil)

(autoload 'org-calendar-goto-agenda "org-agenda" "\
Compute the Org agenda for the calendar date displayed at the cursor.
This is a command that has to be installed in `calendar-mode-map'.

\(fn)" t nil)

(autoload 'org-agenda-to-appt "org-agenda" "\
Activate appointments found in `org-agenda-files'.

With a `\\[universal-argument]' prefix, refresh the list of appointments.

If FILTER is t, interactively prompt the user for a regular
expression, and filter out entries that don't match it.

If FILTER is a string, use this string as a regular expression
for filtering entries out.

If FILTER is a function, filter out entries against which
calling the function returns nil.  This function takes one
argument: an entry from `org-agenda-get-day-entries'.

FILTER can also be an alist with the car of each cell being
either `headline' or `category'.  For example:

  \\='((headline \"IMPORTANT\")
    (category \"Work\"))

will only add headlines containing IMPORTANT or headlines
belonging to the \"Work\" category.

ARGS are symbols indicating what kind of entries to consider.
By default `org-agenda-to-appt' will use :deadline*, :scheduled*
\(i.e., deadlines and scheduled items with a hh:mm specification)
and :timestamp entries.  See the docstring of `org-diary' for
details and examples.

If an entry has a APPT_WARNTIME property, its value will be used
to override `appt-message-warning-time'.

\(fn &optional REFRESH FILTER &rest ARGS)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-agenda" "../../../../../../.emacs.d/elpa/org-9.3.6/org-agenda.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-agenda.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-agenda" '("org-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-archive"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-archive.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-archive.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-archive" '("org-a")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-attach" "../../../../../../.emacs.d/elpa/org-9.3.6/org-attach.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-attach.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-attach" '("org-attach-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-attach-git"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-attach-git.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-attach-git.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-attach-git" '("org-attach-git-")))

;;;***

;;;### (autoloads nil "org-capture" "../../../../../../.emacs.d/elpa/org-9.3.6/org-capture.el"
;;;;;;  "55528a1cdb3bc5a6c8fbcccf1ad547de")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-capture.el

(autoload 'org-capture-string "org-capture" "\
Capture STRING with the template selected by KEYS.

\(fn STRING &optional KEYS)" t nil)

(autoload 'org-capture "org-capture" "\
Capture something.
\\<org-capture-mode-map>
This will let you select a template from `org-capture-templates', and
then file the newly captured information.  The text is immediately
inserted at the target location, and an indirect buffer is shown where
you can edit it.  Pressing `\\[org-capture-finalize]' brings you back to the previous
state of Emacs, so that you can continue your work.

When called interactively with a `\\[universal-argument]' prefix argument GOTO, don't
capture anything, just go to the file/headline where the selected
template stores its notes.

With a `\\[universal-argument] \\[universal-argument]' prefix argument, go to the last note stored.

When called with a `C-0' (zero) prefix, insert a template at point.

When called with a `C-1' (one) prefix, force prompting for a date when
a datetree entry is made.

ELisp programs can set KEYS to a string associated with a template
in `org-capture-templates'.  In this case, interactive selection
will be bypassed.

If `org-capture-use-agenda-date' is non-nil, capturing from the
agenda will use the date at point as the default date.  Then, a
`C-1' prefix will tell the capture process to use the HH:MM time
of the day at point (if any) or the current HH:MM time.

\(fn &optional GOTO KEYS)" t nil)

(autoload 'org-capture-import-remember-templates "org-capture" "\
Set `org-capture-templates' to be similar to `org-remember-templates'.

\(fn)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-capture"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-capture.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-capture.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-capture" '("org-capture-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-clock" "../../../../../../.emacs.d/elpa/org-9.3.6/org-clock.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-clock.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-clock" '("org-")))

;;;***

;;;### (autoloads nil "org-colview" "../../../../../../.emacs.d/elpa/org-9.3.6/org-colview.el"
;;;;;;  "42f4122f97c1f83293112b38c1b0c725")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-colview.el

(autoload 'org-columns-remove-overlays "org-colview" "\
Remove all currently active column overlays.

\(fn)" t nil)

(autoload 'org-columns-get-format-and-top-level "org-colview" "\


\(fn)" nil nil)

(autoload 'org-columns "org-colview" "\
Turn on column view on an Org mode file.

Column view applies to the whole buffer if point is before the
first headline.  Otherwise, it applies to the first ancestor
setting \"COLUMNS\" property.  If there is none, it defaults to
the current headline.  With a `\\[universal-argument]' prefix argument, turn on column
view for the whole buffer unconditionally.

When COLUMNS-FMT-STRING is non-nil, use it as the column format.

\(fn &optional GLOBAL COLUMNS-FMT-STRING)" t nil)

(autoload 'org-columns-compute "org-colview" "\
Summarize the values of PROPERTY hierarchically.
Also update existing values for PROPERTY according to the first
column specification.

\(fn PROPERTY)" t nil)

(autoload 'org-dblock-write:columnview "org-colview" "\
Write the column view table.

PARAMS is a property list of parameters:

`:id' (mandatory)

    The ID property of the entry where the columns view should be
    built.  When the symbol `local', call locally.  When `global'
    call column view with the cursor at the beginning of the
    buffer (usually this means that the whole buffer switches to
    column view).  When \"file:path/to/file.org\", invoke column
    view at the start of that file.  Otherwise, the ID is located
    using `org-id-find'.

`:exclude-tags'

    List of tags to exclude from column view table.

`:format'

    When non-nil, specify the column view format to use.

`:hlines'

    When non-nil, insert a hline before each item.  When
    a number, insert a hline before each level inferior or equal
    to that number.

`:indent'

    When non-nil, indent each ITEM field according to its level.

`:match'

    When set to a string, use this as a tags/property match filter.

`:maxlevel'

    When set to a number, don't capture headlines below this level.

`:skip-empty-rows'

    When non-nil, skip rows where all specifiers other than ITEM
    are empty.

`:vlines'

    When non-nil, make each column a column group to enforce
    vertical lines.

\(fn PARAMS)" nil nil)

(autoload 'org-columns-insert-dblock "org-colview" "\
Create a dynamic block capturing a column view table.

\(fn)" t nil)

(autoload 'org-agenda-columns "org-colview" "\
Turn on or update column view in the agenda.

\(fn)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-colview"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-colview.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-colview.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-colview" '("org-")))

;;;***

;;;***

;;;### (autoloads nil "org-compat" "../../../../../../.emacs.d/elpa/org-9.3.6/org-compat.el"
;;;;;;  "097127ef5dc29136be3a0bebb8850219")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-compat.el

(autoload 'org-check-version "org-compat" "\
Try very hard to provide sensible version strings.

\(fn)" nil t)

;;;### (autoloads "actual autoloads are elsewhere" "org-compat" "../../../../../../.emacs.d/elpa/org-9.3.6/org-compat.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-compat.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-compat" '("org-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-crypt" "../../../../../../.emacs.d/elpa/org-9.3.6/org-crypt.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-crypt.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-crypt" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-ctags" "../../../../../../.emacs.d/elpa/org-9.3.6/org-ctags.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-ctags.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-ctags" '("org-ctags-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-datetree"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-datetree.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-datetree.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-datetree" '("org-datetree-")))

;;;***

;;;### (autoloads nil "org-duration" "../../../../../../.emacs.d/elpa/org-9.3.6/org-duration.el"
;;;;;;  "25d1e7417bea5b0677537075269dec2b")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-duration.el

(autoload 'org-duration-set-regexps "org-duration" "\
Set duration related regexps.

\(fn)" t nil)

(autoload 'org-duration-p "org-duration" "\
Non-nil when string S is a time duration.

\(fn S)" nil nil)

(autoload 'org-duration-to-minutes "org-duration" "\
Return number of minutes of DURATION string.

When optional argument CANONICAL is non-nil, ignore
`org-duration-units' and use standard time units value.

A bare number is translated into minutes.  The empty string is
translated into 0.0.

Return value as a float.  Raise an error if duration format is
not recognized.

\(fn DURATION &optional CANONICAL)" nil nil)

(autoload 'org-duration-from-minutes "org-duration" "\
Return duration string for a given number of MINUTES.

Format duration according to `org-duration-format' or FMT, when
non-nil.

When optional argument CANONICAL is non-nil, ignore
`org-duration-units' and use standard time units value.

Raise an error if expected format is unknown.

\(fn MINUTES &optional FMT CANONICAL)" nil nil)

(autoload 'org-duration-h:mm-only-p "org-duration" "\
Non-nil when every duration in TIMES has \"H:MM\" or \"H:MM:SS\" format.

TIMES is a list of duration strings.

Return nil if any duration is expressed with units, as defined in
`org-duration-units'.  Otherwise, if any duration is expressed
with \"H:MM:SS\" format, return `h:mm:ss'.  Otherwise, return
`h:mm'.

\(fn TIMES)" nil nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-duration"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-duration.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-duration.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-duration" '("org-duration-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-element"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-element.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-element.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-element" '("org-element-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-entities"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-entities.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-entities.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-entities" '("org-entit")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-faces" "../../../../../../.emacs.d/elpa/org-9.3.6/org-faces.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-faces.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-faces" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-feed" "../../../../../../.emacs.d/elpa/org-9.3.6/org-feed.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-feed.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-feed" '("org-feed-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-footnote"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-footnote.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-footnote.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-footnote" '("org-footnote-")))

;;;***

;;;### (autoloads nil "org-goto" "../../../../../../.emacs.d/elpa/org-9.3.6/org-goto.el"
;;;;;;  "a123f14288e9e8b328dd4276ec7bfa87")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-goto.el

(autoload 'org-goto-location "org-goto" "\
Let the user select a location in current buffer.
This function uses a recursive edit.  It returns the selected
position or nil.

\(fn &optional BUF HELP)" nil nil)

(autoload 'org-goto "org-goto" "\
Look up a different location in the current file, keeping current visibility.

When you want look-up or go to a different location in a
document, the fastest way is often to fold the entire buffer and
then dive into the tree.  This method has the disadvantage, that
the previous location will be folded, which may not be what you
want.

This command works around this by showing a copy of the current
buffer in an indirect buffer, in overview mode.  You can dive
into the tree in that copy, use org-occur and incremental search
to find a location.  When pressing RET or `Q', the command
returns to the original buffer in which the visibility is still
unchanged.  After RET it will also jump to the location selected
in the indirect buffer and expose the headline hierarchy above.

With a prefix argument, use the alternative interface: e.g., if
`org-goto-interface' is `outline' use `outline-path-completion'.

\(fn &optional ALTERNATIVE-INTERFACE)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-goto" "../../../../../../.emacs.d/elpa/org-9.3.6/org-goto.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-goto.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-goto" '("org-goto-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-habit" "../../../../../../.emacs.d/elpa/org-9.3.6/org-habit.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-habit.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-habit" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-id" "../../../../../../.emacs.d/elpa/org-9.3.6/org-id.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-id.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-id" '("org-id-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-indent" "../../../../../../.emacs.d/elpa/org-9.3.6/org-indent.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-indent.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-indent" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-inlinetask"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-inlinetask.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-inlinetask.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-inlinetask" '("org-inlinetask-")))

;;;***

;;;### (autoloads nil "org-keys" "../../../../../../.emacs.d/elpa/org-9.3.6/org-keys.el"
;;;;;;  "3a44862d73630586413b1284f30aa25c")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-keys.el

(autoload 'org-babel-describe-bindings "org-keys" "\
Describe all keybindings behind `org-babel-key-prefix'.

\(fn)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-keys" "../../../../../../.emacs.d/elpa/org-9.3.6/org-keys.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-keys.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-keys" '("org-")))

;;;***

;;;***

;;;### (autoloads nil "org-lint" "../../../../../../.emacs.d/elpa/org-9.3.6/org-lint.el"
;;;;;;  "2919a03a3f75ef783297352a4f487156")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-lint.el

(autoload 'org-lint "org-lint" "\
Check current Org buffer for syntax mistakes.

By default, run all checkers.  With a `\\[universal-argument]' prefix ARG, select one
category of checkers only.  With a `\\[universal-argument] \\[universal-argument]' prefix, run one precise
checker by its name.

ARG can also be a list of checker names, as symbols, to run.

\(fn &optional ARG)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-lint" "../../../../../../.emacs.d/elpa/org-9.3.6/org-lint.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-lint.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-lint" '("org-lint-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-list" "../../../../../../.emacs.d/elpa/org-9.3.6/org-list.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-list.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-list" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-macro" "../../../../../../.emacs.d/elpa/org-9.3.6/org-macro.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-macro.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-macro" '("org-macro-")))

;;;***

;;;### (autoloads nil "org-macs" "../../../../../../.emacs.d/elpa/org-9.3.6/org-macs.el"
;;;;;;  "b46cac361e897503c4e65511373e6dc7")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-macs.el

(autoload 'org-load-noerror-mustsuffix "org-macs" "\
Load FILE with optional arguments NOERROR and MUSTSUFFIX.

\(fn FILE)" nil t)

;;;### (autoloads "actual autoloads are elsewhere" "org-macs" "../../../../../../.emacs.d/elpa/org-9.3.6/org-macs.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-macs.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-macs" '("org-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-mobile" "../../../../../../.emacs.d/elpa/org-9.3.6/org-mobile.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-mobile.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-mobile" '("org-mobile-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-mouse" "../../../../../../.emacs.d/elpa/org-9.3.6/org-mouse.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-mouse.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-mouse" '("org-mouse-")))

;;;***

;;;### (autoloads nil "org-num" "../../../../../../.emacs.d/elpa/org-9.3.6/org-num.el"
;;;;;;  "36f2eefd1c75b08b4eaa723e08254649")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-num.el

(autoload 'org-num-default-format "org-num" "\
Default numbering display function.
NUMBERING is a list of numbers.

\(fn NUMBERING)" nil nil)

(autoload 'org-num-mode "org-num" "\
Dynamic numbering of headlines in an Org buffer.

\(fn &optional ARG)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-num" "../../../../../../.emacs.d/elpa/org-9.3.6/org-num.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-num.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-num" '("org-num-")))

;;;***

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-pcomplete"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-pcomplete.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-pcomplete.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-pcomplete" '("pcomplete/org-mode/" "org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-plot" "../../../../../../.emacs.d/elpa/org-9.3.6/org-plot.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-plot.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-plot" '("org-plot")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-protocol"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-protocol.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-protocol.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-protocol" '("org-protocol-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-src" "../../../../../../.emacs.d/elpa/org-9.3.6/org-src.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-src.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-src" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-table" "../../../../../../.emacs.d/elpa/org-9.3.6/org-table.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-table.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-table" '("org")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-tempo" "../../../../../../.emacs.d/elpa/org-9.3.6/org-tempo.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-tempo.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-tempo" '("org-tempo-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-timer" "../../../../../../.emacs.d/elpa/org-9.3.6/org-timer.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-timer.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-timer" '("org-timer-")))

;;;***

;;;### (autoloads nil "org-version" "../../../../../../.emacs.d/elpa/org-9.3.6/org-version.el"
;;;;;;  "724c540ffaa98127f0584182f8b3cbcc")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/org-version.el

(autoload 'org-release "org-version" "\
The release version of Org.
Inserted by installing Org mode or when a release is made.

\(fn)" nil nil)

(autoload 'org-git-version "org-version" "\
The Git version of Org mode.
Inserted by installing Org or when a release is made.

\(fn)" nil nil)

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox" "../../../../../../.emacs.d/elpa/org-9.3.6/ox.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox" '("org-export-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-ascii" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-ascii.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-ascii.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-ascii" '("org-ascii-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-beamer" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-beamer.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-beamer.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-beamer" '("org-beamer-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-html" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-html.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-html.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-html" '("org-html-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-icalendar"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ox-icalendar.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-icalendar.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-icalendar" '("org-icalendar-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-latex" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-latex.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-latex.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-latex" '("org-latex-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-man" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-man.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-man.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-man" '("org-man-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-md" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-md.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-md.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-md" '("org-md-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-odt" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-odt.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-odt.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-odt" '("org-odt-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-org" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-org.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-org.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-org" '("org-org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-publish" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-publish.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-publish.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-publish" '("org-publish-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-texinfo" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-texinfo.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-9.3.6/ox-texinfo.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-texinfo" '("org-texinfo-")))

;;;***

;;;### (autoloads nil nil ("../../../../../../.emacs.d/elpa/org-9.3.6/ob-C.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-J.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-R.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-abc.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-asymptote.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-awk.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-calc.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-clojure.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-comint.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-coq.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-core.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-css.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ditaa.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-dot.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ebnf.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-emacs-lisp.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-eshell.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-eval.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-exp.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-forth.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-fortran.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-gnuplot.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-groovy.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-haskell.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-hledger.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-io.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-java.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-js.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-latex.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ledger.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-lilypond.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-lisp.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-lob.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-lua.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-makefile.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-matlab.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-maxima.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-mscgen.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ocaml.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-octave.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-org.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-perl.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-picolisp.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-plantuml.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-processing.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-python.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ref.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-ruby.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-sass.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-scheme.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-screen.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-sed.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-shell.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-shen.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-sql.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-sqlite.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-stan.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob-table.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-tangle.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ob-vala.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ob.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ol-bbdb.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-bibtex.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ol-docview.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ol-eshell.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ol-eww.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-gnus.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ol-info.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-irc.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ol-mhe.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ol-rmail.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ol-w3m.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ol.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-agenda.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-archive.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-attach-git.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-attach.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-autoloads.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-capture.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-clock.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-colview.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-compat.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-crypt.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-ctags.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-datetree.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-duration.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-element.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-entities.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-faces.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-feed.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-footnote.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-goto.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-habit.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-id.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-indent.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-inlinetask.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-install.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-keys.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-lint.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-list.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-loaddefs.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-macro.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-macs.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-mobile.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-mouse.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-num.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-pcomplete.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-pkg.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-plot.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-protocol.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-src.el" "../../../../../../.emacs.d/elpa/org-9.3.6/org-table.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-tempo.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-timer.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org-version.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/org.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-ascii.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ox-beamer.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ox-html.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-icalendar.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ox-latex.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-man.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ox-md.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-odt.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ox-org.el" "../../../../../../.emacs.d/elpa/org-9.3.6/ox-publish.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ox-texinfo.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-9.3.6/ox.el") (0 0 0
;;;;;;  0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-autoloads.el ends here
