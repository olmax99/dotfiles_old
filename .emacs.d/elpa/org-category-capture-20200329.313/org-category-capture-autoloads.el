;;; org-category-capture-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads "actual autoloads are elsewhere" "org-category-capture"
;;;;;;  "../../../../../../.emacs.d/elpa/org-category-capture-20200329.313/org-category-capture.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-category-capture-20200329.313/org-category-capture.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-category-capture" '("occ-")))

;;;***

;;;### (autoloads nil nil ("../../../../../../.emacs.d/elpa/org-category-capture-20200329.313/org-category-capture-autoloads.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-category-capture-20200329.313/org-category-capture.el")
;;;;;;  (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-category-capture-autoloads.el ends here
