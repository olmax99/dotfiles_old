;;; org-projectile-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "org-projectile" "../../../../../../.emacs.d/elpa/org-projectile-20190130.1439/org-projectile.el"
;;;;;;  "e657343eccdaa178c336efc5e80d7a95")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-projectile-20190130.1439/org-projectile.el

(autoload 'org-projectile-goto-location-for-project "org-projectile" "\
Goto the location at which TODOs for PROJECT are stored.

\(fn PROJECT)" t nil)

(autoload 'org-projectile-single-file "org-projectile" "\
Set `org-projectile-strategy' so that captures occur in a single file.

\(fn)" t nil)

(autoload 'org-projectile-per-project "org-projectile" "\
Set `org-projectile-strategy' so that captures occur within each project.

\(fn)" t nil)

(autoload 'org-projectile-project-todo-completing-read "org-projectile" "\
Select a project using a `projectile-completing-read' and record a TODO.

If CAPTURE-TEMPLATE is provided use it as the capture template
for the TODO. ADDITIONAL-OPTIONS will be supplied as though they
were part of the capture template definition.

\(fn &rest ADDITIONAL-OPTIONS &key CAPTURE-TEMPLATE &allow-other-keys)" t nil)

(autoload 'org-projectile-capture-for-current-project "org-projectile" "\
Capture a TODO for the current active projectile project.

If CAPTURE-TEMPLATE is provided use it as the capture template
for the TODO. ADDITIONAL-OPTIONS will be supplied as though they
were part of the capture template definition.

\(fn &rest ADDITIONAL-OPTIONS &key CAPTURE-TEMPLATE &allow-other-keys)" t nil)

;;;### (autoloads "actual autoloads are elsewhere" "org-projectile"
;;;;;;  "../../../../../../.emacs.d/elpa/org-projectile-20190130.1439/org-projectile.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/org-projectile-20190130.1439/org-projectile.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-projectile" '("occ-" "org-projectile-")))

;;;***

;;;***

;;;### (autoloads nil nil ("../../../../../../.emacs.d/elpa/org-projectile-20190130.1439/org-projectile-autoloads.el"
;;;;;;  "../../../../../../.emacs.d/elpa/org-projectile-20190130.1439/org-projectile.el")
;;;;;;  (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-projectile-autoloads.el ends here
