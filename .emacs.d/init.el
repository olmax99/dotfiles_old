(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;;;;
;; Packages
;;;;
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

;; Define package repositories
;; (require 'package)
;; (add-to-list 'package-archives
;;              '("tromey" . "https://tromey.com/elpa/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives 
             '("melpa" . "https://melpa.org/packages/") t)


(add-to-list 'package-pinned-packages '(cider . "melpa-stable") t)
(add-to-list 'package-pinned-packages '(magit . "melpa-stable") t)

;; Load and activate emacs packages. Do this first so that the
;; packages are loaded before you start trying to modify them.
;; This also sets the load path.
(package-initialize)

;; Download the ELPA archive description if needed.
;; This informs Emacs about the latest versions of all packages, and
;; makes them available for download.
(when (not package-archive-contents)
  (package-refresh-contents))

;; Define he following variables to remove the compile-log warnings
;; when defining ido-ubiquitous
;; (defvar ido-cur-item nil)
;; (defvar ido-default-item nil)
;; (defvar ido-cur-list nil)
;; (defvar predicate nil)
;; (defvar inherit-input-method nil)

;; The packages you want installed. You can also install these
;; manually with M-x package-install
;; Add in your own as you wish:
(defvar my-packages
  '(;; makes handling lisp expressions much, much easier
    ;; Cheatsheet: http://www.emacswiki.org/emacs/PareditCheatsheet
    paredit

    ;; key bindings and code colorization for Clojure
    ;; https://github.com/clojure-emacs/clojure-mode
    clojure-mode

    ;; extra syntax highlighting for clojure
    clojure-mode-extra-font-locking

    ;; integration with a Clojure REPL
    ;; https://github.com/clojure-emacs/cider
    cider

    ;; go-mode and official gopls client
    go-mode
    lsp-mode
    lsp-ui

    markdown-mode

    ;; allow ido usage in as many contexts as possible. see
    ;; customizations/navigation.el line 23 for a description
    ;; of ido
    ido-completing-read+

    ;; Enhances M-x to allow easier execution of commands. Provides
    ;; a filterable list of possible commands in the minibuffer
    ;; http://www.emacswiki.org/emacs/Smex
    smex

    ;; project navigation
    projectile

    ;; colorful parenthesis matching
    rainbow-delimiters

    ;; edit html tags like sexps
    tagedit

    ;; terraform support
    terraform-mode

    ;; yaml support
    yaml-mode
    indent-tools

    ;; ripgrep
    rg

    ;; advanced org-mode search
    helm-org-rifle

    ;; Dockerfile
    dockerfile-mode
    ))

;; On OS X, an Emacs instance started from the graphical user
;; interface will have a different environment than a shell in a
;; terminal window, because OS X does not run a shell during the
;; login. Obviously this will lead to unexpected results when
;; calling external utilities like make from Emacs.
;; This library works around this problem by copying important
;; environment variables from the user's shell.
;; https://github.com/purcell/exec-path-from-shell
(if (eq system-type 'darwin)
    (add-to-list 'my-packages 'exec-path-from-shell))

(dolist (p my-packages)
    (when (not (package-installed-p p))
      (package-install p)))


;; preferred way of checking, installing and configuring packages
;; (use-package company
;;   :ensure t
;;   :bind (("M-a". company-complete))
;;   :config
;;   (global-company-mode))

;; custom binding for git integration
(use-package magit
  :ensure t
  :config
  (global-set-key (kbd "C-c m") 'magit-status))

;;;; 
;; Programming
;;;;

;; ---------------------- Python for Emacs----------------------------
(use-package elpy
  :ensure t
  :init
  (elpy-enable)) 

;;; --------------------- Golang--------------------------------------
;;; https://github.com/golang/tools/blob/master/gopls/doc/emacs.md
;;; needs to know the PATH
(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (replace-regexp-in-string
                          "[ \t\n]*$"
                          ""
                          (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq eshell-path-env path-from-shell) ; for eshell users
    (setq exec-path (split-string path-from-shell path-separator))))

;;; Go needs to know where the Go code goes - handled by gvm
;;; Everytime a file in go-mode is saved, the Go-formatter will do its magic
(defun set-gopath-from-shell-GOPATH ()
  let ((gopath-from-shell (replace-regexp-in-string
                           "[ \t\n]*$"
                           ""
                           (shell-command-to-string "$SHELL --login -i -c 'echo $GOPATH'"))))
  (setenv "GOPATH" gopath-from-shell)
  (add-to-list 'exec-path gopath-from-shell))


(use-package lsp-mode
  :ensure t
  :commands (lsp lsp-deferred)
  :hook (go-mode . lsp-deferred))

;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Optional - provides fancier overlays.
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

;;;;
;;  IDE Helpers
;;;;

;; Company mode is a standard completion package that works well with lsp-mode.
(use-package company
  :ensure t
  :bind (("M-a". company-complete))
  :config
  ;; Optionally enable completion-as-you-type behavior.
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 1)
  (global-company-mode))

;; Optional - provides snippet support.
(use-package yasnippet
  :ensure t
  :commands yas-minor-mode
  :hook (go-mode . yas-minor-mode)) 

;; toggle directory tree of current .git folder as parent
(use-package neotree
  :config
  (defun neotree-project-dir ()
    "Open NeoTree using the git root."
    (interactive)
    (let ((project-dir (projectile-project-root))
          (file-name (buffer-file-name)))
      (neotree-toggle)
      (if project-dir
          (if (neo-global--window-exists-p)
              (progn
                (neotree-dir project-dir)
                (neotree-find file-name)))
        (message "Could not find git project root."))))
  (global-set-key [f8] 'neotree-project-dir))

;;;;
;; Org-mode - more details https://gitlab.com/spudlyo/dotfiles/-/blob/master/emacs/.emacs.d/init.el
;;;;

(use-package org
  :commands
  org-indent-mode org-babel-lob-ingest org-html-publish-to-html
  org-publish org-ascii-publish-to-ascii
  :init
  (defvar my-org-dir "~/s3olmax/org")
  (defun my-org-mode-hook ()
    "Stuff to do when entering org mode."
    (setq org-fontify-done-headline t)
    (set-face-attribute 'org-done nil :strike-through t)
    (set-face-attribute 'org-headline-done nil
                        :strike-through t
                        :foreground "light gray")
    (turn-on-auto-fill)
    (turn-on-flyspell)
    (org-indent-mode 1)
    (setq fill-column 80))
  (defun my-org-confirm-babel-evaluate (lang body)
    "Don't confirm squat."
    (not (member lang '("sh" "python" "elisp" "ruby" "shell" "dot"
                        "perl"))))
  :config
  (setq org-directory my-org-dir
        org-startup-indented t
        org-babel-min-lines-for-block-output 1
        org-startup-folded "showeverything"
        org-startup-with-inline-images t
        org-src-preserve-indentation t
        org-use-speed-commands t
        org-hide-emphasis-markers t
        org-export-with-section-numbers nil
        org-export-with-toc t
        org-export-with-date nil
        org-export-time-stamp-file nil
        org-export-with-email t
        org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate
        org-babel-default-header-args
          (cons '(:noweb . "yes")
                (assq-delete-all :noweb org-babel-default-header-args))
        org-babel-default-header-args
          (cons '(:exports . "both")
                (assq-delete-all :exports org-babel-default-header-args))
        org-babel-default-header-args
          (cons '(:results . "output verbatim replace")
                (assq-delete-all :results org-babel-default-header-args)))
  (custom-set-faces '(org-ellipsis ((t (:foreground "gray40" :underline nil)))))
  (global-set-key (kbd "C-c c") 'org-capture)
  (global-set-key (kbd "C-c l") 'org-store-link)
  (global-set-key (kbd "C-c a") 'org-agenda)
  (org-babel-do-load-languages 'org-babel-load-languages
   '((shell      . t)
     (python     . t)
     (ruby       . t)
     (perl       . t)
     (emacs-lisp . t)
     (dot        . t)))
  :hook
  (org-mode . my-org-mode-hook))

;;; Custom org-capture templates
(use-package org-capture
  :config
  (setq org-capture-templates
        '(("n" "Notes" entry 
           (file+headline "notes.org" "..::: NOTES :::..")
           "* TODO %?\n :CAPTURED: %U\n :LINK: %a %i")
          ("b" "Org-Brain Topics - store dired-org-link before creating a new entry" entry
           (file+headline "topics.org" "..::: NEW ORG-BRAIN-TOPICS :::..")
           (file "templates/org-brain-template"))))
  :bind ("C-c c" . org-capture))

;;; Org-brain for mindmapping
(use-package org-brain 
  :ensure t
  :init
  (setq org-brain-path "~/s3olmax/org/brain")
  ;; For Evil users
  ;; (with-eval-after-load 'evil
  ;;   (evil-set-initial-state 'org-brain-visualize-mode 'emacs))
  :config
  (global-set-key (kbd "C-c v") 'org-brain-visualize)
  (bind-key "C-c b" 'org-brain-prefix-map org-mode-map)
  (setq org-id-track-globally t)
  ;; (setq org-id-locations-file "~/.emacs.d/.org-id-locations")
  ;; (add-hook 'before-save-hook #'org-brain-ensure-ids-in-buffer)
  ;; (push '("b" "Brain" plain (function org-brain-goto-end)
  ;;         "* %i%?" :empty-lines 1)
  ;;       org-capture-templates)
  ;; (setq org-brain-visualize-default-choices 'all)
  ;; (setq org-brain-title-max-length 12)
  ;; (setq org-brain-include-file-entries nil
  ;;       org-brain-file-entries-use-title nil)
)

;;; Allows you to edit entries directly from org-brain-visualize
;;; Breaks Emacs: Error (use-package): polymode/:catch: Symbol’s value as variable is void: org-brain-poly-hostmode
;; (use-package polymode
;;   :config
;;   (add-hook 'org-brain-visualize-mode-hook #'org-brain-polymode))

;;; TODO Move cloudformation.el, etc. to vendor
;; Place downloaded elisp files in ~/.emacs.d/vendor. You'll then be able
;; to load them.
;;
;; For example, if you download yaml-mode.el to ~/.emacs.d/vendor,
;; then you can add the following code to this file:
;;
;; (require 'yaml-mode)
;; (add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
;; 
;; Adding this code will make Emacs enter yaml mode whenever you open
;; a .yml file
(add-to-list 'load-path "~/.emacs.d/vendor") 

;;; Combine projectile with org-capture for source code todos
(use-package org-projectile
  :init
  (defvar my-org-file "~/s3olmax/org/projects.org")
  :defer 3
  :bind ("C-c n p" . org-projectile-project-todo-completing-read)
  :config
  (progn
    (setq org-projectile-projects-file my-org-file
          ;; org-agenda-files (append org-agenda-files (org-projectile-todo-files))
          )
    ;; (push (org-projectile-project-todo-entry) org-capture-templates)
    (add-to-list 'org-capture-templates
                 (org-projectile-project-todo-entry
                  :capture-character "p"))
    (setq org-confirm-elisp-link-function nil))
  :ensure t)

;;;;
;; Customization
;;;;

;; Add a directory to our load path so that when you `load` things
;; below, Emacs knows where to look for the corresponding file.
(add-to-list 'load-path "~/.emacs.d/customizations")

;; Sets up exqec-path-from-shell so that Emacs will use the correct
;; environment variables
(load "shell-integration.el")

;; These customizations make it easier for you to navigate files,
;; switch buffers, and choose options from the minibuffer.
(load "navigation.el")

;; These customizations change the way emacs looks and disable/enable
;; some user interface elements
(load "ui.el")

;; These customizations make editing a bit nicer.
(load "editing.el")

;; Hard-to-categorize customizations
(load "misc.el")

;; For editing lisps
(load "elisp-editing.el")

;; highlight indentations
(load "highlight-indentation.el")

;; Language-specific
(load "setup-clojure.el")
(load "setup-js.el")
(load "cloudformation-mode.el")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#eaeaea" "#d54e53" "#b9ca4a" "#e7c547" "#7aa6da" "#c397d8" "#70c0b1" "#000000"))
 '(coffee-tab-width 2)
 '(custom-enabled-themes (quote (material)))
 '(custom-safe-themes
   (quote
    ("d4f8fcc20d4b44bf5796196dbeabec42078c2ddb16dcb6ec145a1c610e0842f3" "7f1263c969f04a8e58f9441f4ba4d7fb1302243355cb9faecb55aec878a06ee9" "a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" "732b807b0543855541743429c9979ebfb363e27ec91e82f463c91e68c772f6e3" "36ca8f60565af20ef4f30783aa16a26d96c02df7b4e54e9900a5138fb33808da" "bf798e9e8ff00d4bf2512597f36e5a135ce48e477ce88a0764cfb5d8104e8163" "c9ddf33b383e74dac7690255dd2c3dfa1961a8e8a1d20e401c6572febef61045" "973c3250a04a34d7f4f7db2576d19f333ecd6b59ab40c2b9772b007d486b6ab0" "cf08ae4c26cacce2eebff39d129ea0a21c9d7bf70ea9b945588c1c66392578d1" "52588047a0fe3727e3cd8a90e76d7f078c9bd62c0b246324e557dfa5112e0d0c" "9e54a6ac0051987b4296e9276eecc5dfb67fdcd620191ee553f40a9b6d943e78" "1157a4055504672be1df1232bed784ba575c60ab44d8e6c7b3800ae76b42f8bd" "5ee12d8250b0952deefc88814cf0672327d7ee70b16344372db9460e9a0e3ffc" default)))
 '(fci-rule-color "#2a2a2a")
 '(magit-status-sections-hook
   (quote
    (magit-insert-status-headers magit-insert-merge-log magit-insert-rebase-sequence magit-insert-am-sequence magit-insert-sequencer-sequence magit-insert-bisect-output magit-insert-bisect-rest magit-insert-bisect-log magit-insert-untracked-files magit-insert-unstaged-changes magit-insert-staged-changes magit-insert-stashes magit-insert-unpushed-to-pushremote magit-insert-unpushed-to-upstream-or-recent magit-insert-unpulled-from-pushremote magit-insert-unpulled-from-upstream magit-insert-ignored-files)))
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
    (indent-tools helm-org-rifle polymode org-brain org-bookmark-heading smooth-scrolling rg terraform-mode orgtbl-show-header elpy yaml-mode lsp-mode neotree material-theme use-package company ample-theme markdown-mode cider-eval-sexp-fu vs-dark-theme cider-hydra magit tagedit rainbow-delimiters projectile smex ido-completing-read+ cider clojure-mode-extra-font-locking clojure-mode paredit)))
 '(tool-bar-mode nil))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 1 :width normal :foundry "default" :family "default"))))
 '(org-ellipsis ((t (:foreground "gray40" :underline nil)))))
